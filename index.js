export const DEBOUNCE_BTN = document.querySelector('.debounce');
export const THROTTLE_BTN = document.querySelector('.throttle');
const PLAYGROUND = document.querySelector('.playground');
export const RANGE = document.querySelector('.set-timer');
const OUTPUT = document.querySelector('output');
RANGE.addEventListener('change', () => {
  OUTPUT.innerHTML = RANGE.value + 'ms';
});
export function createShape(shape) {
  const element = document.createElement('div');
  const dims = PLAYGROUND.getBoundingClientRect();
  element.classList.add('shape', shape);
  const random = Math.floor(Math.random() * (560 - 140)) + 140;
  element.style.top = `${random}px`;
  PLAYGROUND.append(element);
  setTimeout(() => PLAYGROUND.removeChild(element), 5000);
}
let id;
export function timer(el, time) {
  const timeStart = Date.now();
  const timeEnd = timeStart + time;
  let difference = timeEnd - timeStart;
  clearInterval(id);
  id = setInterval(() => {
    if (difference > 0) {
      difference -= 100;
      const ss = Math.floor(difference / 1000);
      const ms = difference % 1000;
      el.innerHTML = `${ss}.${ms.toString()[0]}`;
    } else {
      el.innerHTML = 'READY!';
      clearInterval(id);
    }
  }, 100);
}
