import { createShape, THROTTLE_BTN, timer, RANGE } from './index';
const COUNT = THROTTLE_BTN.querySelector('small');
let count = 0;
function throttle(fn, ms) {
  let flag = false;
  return function(...args) {
    if (!flag) {
      fn.apply(this, args);
      flag = true;
      setTimeout(() => {
        flag = false;
        count = 0;
        COUNT.innerHTML = count;
      }, ms);
    }
    count += 1;
    COUNT.innerHTML = count;
  };
}
function handleThrottleClick() {
  timer(document.querySelector('.timer--throttle'), Number(RANGE.value));
  createShape('circle');
}
THROTTLE_BTN.addEventListener(
  'click',
  throttle(handleThrottleClick, Number(RANGE.value)),
);
