# Debounce v. Throttle

**Comparing the differences between debounced and throttled events.**

## Debouncing

> Awaits for a window of time without any events before executing, meaning that as long as a user is clicking the event will not fire until a user stops for a predetermined amount of time.

_Debouncing timer resets every time a user performs an action._

## Throttling

> Counts down a certain amount of time until being able to run the event again. No matter how many times a user clicks a button, the button will be clicked only as often as throttling allows.

_Throttling timer counts down until the next event can be triggered._
