import { createShape, DEBOUNCE_BTN, timer, RANGE } from './index';
const COUNT = DEBOUNCE_BTN.querySelector('small');
let count = 0;
function debounce(fn, ms) {
  let id;
  return function(...args) {
    count += 1;
    COUNT.innerHTML = count;
    clearTimeout(id);
    id = setTimeout(() => fn.apply(this, args), ms);
    //TODO: timer for the user counting down the debounce
    timer(document.querySelector('.timer--debounce'), Number(RANGE.value));
  };
}

function handleDebouncedClick() {
  count = 0;
  COUNT.innerHTML = count;
  createShape('square');
}

DEBOUNCE_BTN.addEventListener(
  'click',
  debounce(handleDebouncedClick, Number(RANGE.value)),
);
